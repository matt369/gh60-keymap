#pragma once

/* USB device descriptor parameters */
#undef MANUFACTURER
#define MANUFACTURER QMK
#undef PRODUCT
#define PRODUCT GH60
#undef DESCRIPTION

/* disable mechanical locking support */
#undef LOCKING_SUPPORT_ENABLE
#undef LOCKING_RESYNC_ENABLE

/* disable action features */
#define NO_ACTION_ONESHOT
#define NO_ACTION_TAPPING

/* disable USB suspend check after keyboard startup */
#define NO_USB_STARTUP_CHECK

/* GRAVE_ESC overrides */
#define GRAVE_ESC_ALT_OVERRIDE
#define GRAVE_ESC_CTRL_OVERRIDE
#define GRAVE_ESC_GUI_OVERRIDE

/* time in milliseconds until a tap becomes a hold */
#define TAPPING_TERM 200
