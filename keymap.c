#include QMK_KEYBOARD_H

#define XXX KC_NO

#define LAYOUT_60_ansi_7u_space( \
	  K00, K01, K02, K03, K04, K05, K06, K07, K08, K09, K0A, K0B, K0C, K0D,   \
	  K10, K11, K12, K13, K14, K15, K16, K17, K18, K19, K1A, K1B, K1C, K1D,   \
	  K20, K21, K22, K23, K24, K25, K26, K27, K28, K29, K2A, K2B,      K2D,   \
	  K30,      K32, K33, K34, K35, K36, K37, K38, K39, K3A, K3B,      K3D,   \
	  K40, K41, K42,           K45,                          K4B, K4C, K4D    \
) { \
	{ K00, K01, K02, K03, K04, K05, K06, K07, K08, K09, K0A, K0B, K0C, K0D }, \
	{ K10, K11, K12, K13, K14, K15, K16, K17, K18, K19, K1A, K1B, K1C, K1D }, \
	{ K20, K21, K22, K23, K24, K25, K26, K27, K28, K29, K2A, K2B, XXX, K2D }, \
	{ K30, XXX, K32, K33, K34, K35, K36, K37, K38, K39, K3A, K3B, XXX, K3D }, \
	{ K40, K41, K42, XXX, XXX, K45, XXX, XXX, XXX, XXX, XXX, K4B, K4C, K4D }  \
}

enum layer_names {
	_BL, /* base layer */
	_AL, /* arrow keys layer */
	_NL, /* numeric keypad layer */
	_FL  /* function layer */
};

enum custom_keycodes {
	FN_ESC = SAFE_RANGE,
	FN_ENT
};

typedef struct {
	bool pressed;
	uint16_t time;
} Key;

static Key fn_esc, fn_ent;

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
	[_BL] = LAYOUT_60_ansi_7u_space( \
		KC_GESC, KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS, KC_EQL,  KC_BSPC, \
		KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_LBRC, KC_RBRC, KC_BSLS, \
		FN_ESC,  KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT,          FN_ENT,  \
		KC_LSFT,          KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH,          KC_RSFT, \
		KC_LCTL, KC_LALT, KC_LGUI,                   KC_SPC,                                               KC_RGUI, KC_RALT, KC_RCTL  \
	),

	[_AL] = LAYOUT_60_ansi_7u_space( \
		_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
		_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
		_______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, \
		_______,          _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          KC_UP,   \
		_______, _______, _______,                   _______,                                              KC_LEFT, KC_DOWN, KC_RGHT  \
	),

	[_NL] = LAYOUT_60_ansi_7u_space( \
		_______, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_P7,   KC_P8,   KC_P9,   KC_PAST, XXXXXXX, XXXXXXX, _______, \
		_______, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_P4,   KC_P5,   KC_P6,   KC_PMNS, XXXXXXX, XXXXXXX, XXXXXXX, \
		_______, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_P1,   KC_P2,   KC_P3,   KC_PPLS, XXXXXXX,          _______, \
		_______,          XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_P0,   XXXXXXX, KC_PDOT, KC_PSLS,          _______, \
		_______, _______, _______,                   _______,                                              _______, _______, _______  \
	),

	[_FL] = LAYOUT_60_ansi_7u_space( \
		KC_GRV,  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  KC_DEL,  \
		_______, XXXXXXX, XXXXXXX, KC_ENT,  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_PSCR, KC_HOME, KC_END,  KC_INS,  \
		_______, TG(_AL), XXXXXXX, TG(_NL), XXXXXXX, XXXXXXX, KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT, XXXXXXX, KC_PGUP,          _______, \
		_______,          XXXXXXX, XXXXXXX, KC_CLCK, XXXXXXX, XXXXXXX, KC_NLCK, KC_APP,  KC_SLCK, KC_PAUS, KC_PGDN,          _______, \
		_______, _______, _______,                   _______,                                              _______, _______, _______  \
	)
};

bool
process_record_user(uint16_t keycode, keyrecord_t *record)
{
	static bool kintrpt;

	switch (keycode) {
	case FN_ESC:
		if (record->event.pressed) {
			layer_on(_FL);
			fn_esc.pressed = true;
			fn_esc.time = record->event.time;
			return kintrpt = false;
		}

		if (!fn_ent.pressed)
			layer_off(_FL);

		if (!kintrpt && timer_elapsed(fn_esc.time) <= TAPPING_TERM) {
			tap_code(KC_ESC);
			kintrpt = true;
		}

		return fn_esc.pressed = false;

	case FN_ENT:
		if (record->event.pressed) {
			fn_ent.pressed = true;
			fn_ent.time = record->event.time;
			return kintrpt = false;
		}

		if (!fn_esc.pressed)
			layer_off(_FL);

		if (!kintrpt || timer_elapsed(fn_ent.time) <= TAPPING_TERM) {
			tap_code(IS_LAYER_ON(_NL) ? KC_PENT : KC_ENT);
			kintrpt = true;
		}

		return fn_ent.pressed = false;
	}

	return kintrpt = true;
}

void
matrix_scan_user(void)
{
	if (fn_ent.pressed && timer_elapsed(fn_ent.time) > TAPPING_TERM)
		layer_on(_FL);
}
